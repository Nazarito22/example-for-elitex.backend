<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    use HasFactory;

    protected $casts = [
        'user_id' => 'integer',
        'stall_status' => 'boolean',
        'stall_id' => 'integer'
    ];

    protected $fillable = [
        'user_id', 'stall_status', 'stall_id'
    ];
}
