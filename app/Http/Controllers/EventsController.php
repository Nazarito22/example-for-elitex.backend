<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Events;

class EventsController extends Controller
{
    public function getEvents()
    {
        $events = Events::query()->orderByDesc('id')->get()->take(50);
        $newEvents = [];

        for ($i = 0; $i < count($events); $i++) {
            $newEvents[] = (object)[
                'id' => $events[$i]['id'],
                'user_id' => $events[$i]['user_id'],
                'user_name' => User::query()->where('id', $events[$i]['user_id'])->value('name'),
                'stall_status' => $events[$i]['stall_status'],
                'stall_id' => $events[$i]['stall_id']
            ];
        }
        return $newEvents;
    }
}
