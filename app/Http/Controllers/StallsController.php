<?php

namespace App\Http\Controllers;

use App\Models\Events;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Stalls;

class StallsController extends Controller
{
    public function getStalls(Request $request)
    {
        $stalls = Stalls::query()->get();
        $stall_id = User::query()->where('id', $request->user_id)->value('stall_id');

        return $this->newStalls($stalls, $stall_id);
    }

    public function takeStall(Request $request)
    {
        Stalls::query()->where('id', $request->stall_id)->update(['user_id' => $request->user_id, 'status' => true]);
        Events::query()->create(['user_id' => $request->user_id, 'stall_status' => true, 'stall_id' => $request->stall_id]);
        User::query()->where('id', $request->user_id)->update(['stall_id' => $request->stall_id]);
        $stall_id = $request->stall_id;
        $stalls = Stalls::query()->get();

        return $this->newStalls($stalls, $stall_id);
    }

    public function releaseStall(Request $request)
    {
        Stalls::query()->where('user_id', $request->user_id)->update(['user_id' => 0, 'status' => false]);
        Events::query()->create(['user_id' => $request->user_id, 'stall_status' => false, 'stall_id' => $request->stall_id]);
        User::query()->where('id', $request->user_id)->update(['stall_id' => 0]);
        $stall_id = 0;

        $stalls = Stalls::query()->get();
        return $this->newStalls($stalls, $stall_id);
    }

    public function newStalls($stalls, $stall_id)
    {
        $newStalls = [];

        for ($i = 0; $i < count($stalls); $i++) {
            $newStalls[] = (object)[
                'id' => $stalls[$i]['id'],
                'user_id' => $stalls[$i]['user_id'],
                'stall_id' => $stalls[$i]['user_id'] === 0 ? 0 : User::query()->where('id', $stalls[$i]['user_id'])->value('stall_id'),
                'name' => $stalls[$i]['user_id'] === 0 ? 'Undefined' : User::query()->where('id', $stalls[$i]['user_id'])->value('name'),
                'status' => $stalls[$i]['status']
            ];
        }

        $res = [
            'currentStallForUser' => $stall_id,
            'stalls' => $newStalls
        ];

        return $res;
    }
}
