<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');
Route::post('signup', 'AuthController@signup');
Route::post('logout', 'AuthController@logout');
Route::post('check-auth', 'AuthController@checkAuth');

Route::post('stalls', 'StallsController@getStalls');
Route::put('stalls/take', 'StallsController@takeStall');
Route::put('stalls/release', 'StallsController@releaseStall');

Route::get('messages', 'ChatController@getMessages');
Route::get('events', 'EventsController@getEvents');

